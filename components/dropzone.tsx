"use client";

import React, { useRef, useState, FunctionComponent } from "react";
import { Button } from "@nextui-org/button";
import { useRouter } from "next/navigation";
import { siteConfig } from "@/config/site";
import { Spinner } from "@nextui-org/spinner";

interface DropFileInputProps {}

const DropFileInput: FunctionComponent<DropFileInputProps> = () => {
  const wrapperRef = useRef<HTMLDivElement>(null);

  const [fileList, setFileList] = useState<File[]>([]);
  const [enabledButton, setEnabledButton] = useState<boolean>(false);
  const router = useRouter();
  const [load, setLoad] = useState(false);
  const onDragEnter = () => wrapperRef.current?.classList.add("dragover");

  const onDragLeave = () => wrapperRef.current?.classList.remove("dragover");

  const onDrop = () => {
    wrapperRef.current?.classList.remove("dragover");
    setEnabledButton(false);
  };

  const handleSaveToDatabase = async () => {
    setLoad(true);
    if (fileList.length === 0) {
      return;
    }

    const formData = new FormData();
    fileList.forEach((file, index) => {
      formData.append(`file${index + 1}`, file);
    });

    try {
      const response = await fetch(
        `${siteConfig.baseUrl}/api/v1/analyzer/upload-pdf/`,
        {
          method: "POST",
          body: formData,
        }
      );

      if (response.ok) {
        const data = await response.json();

        localStorage.removeItem("quiz");
        router.push(`/quiz/?uid=${data.uid}`);
        setLoad(false);
      } else {
        console.error(
          "Error saving files to the database:",
          response.statusText
        );
      }
    } catch (error) {
      console.error("Error saving files to the database:", error);
    }
  };

  const onFileDrop = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newFile = e.target.files && e.target.files[0];
    if (newFile) {
      if (newFile.size > 1024 * 1024 * 4) {
        alert("File size should not exceed 4MB");
        return;
      }
      const updatedList = [...fileList, newFile];
      setFileList(updatedList);
      setEnabledButton(true);
    }
  };

  const fileRemove = (file: File) => {
    const updatedList = fileList.filter((f) => f !== file);
    setFileList(updatedList);
    setEnabledButton(false);
  };

  return (
    <>
      <div
        className="flex items-center justify-center w-full"
        ref={wrapperRef}
        onDragEnter={onDragEnter}
        onDragLeave={onDragLeave}
        onDrop={onDrop}>
        <label
          htmlFor="dropzone-file"
          className="flex flex-col items-center justify-center w-full h-64 border-2 border-gray-300 border-dashed rounded-lg cursor-pointer bg-gray-50 dark:hover:bg-bray-800 dark:bg-gray-700 hover:bg-gray-100 dark:border-gray-600 dark:hover:border-gray-500 dark:hover:bg-gray-600">
          <div className="flex flex-col items-center justify-center pt-5 pb-6">
            <svg
              className="w-8 h-8 mb-4 text-gray-500 dark:text-gray-400"
              aria-hidden="true"
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 20 16">
              <path
                stroke="currentColor"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M13 13h3a3 3 0 0 0 0-6h-.025A5.56 5.56 0 0 0 16 6.5 5.5 5.5 0 0 0 5.207 5.021C5.137 5.017 5.071 5 5 5a4 4 0 0 0 0 8h2.167M10 15V6m0 0L8 8m2-2 2 2"
              />
            </svg>
            <p className="mb-2 text-sm text-gray-500 dark:text-gray-400">
              <span className="font-semibold">Click to upload</span>
            </p>
            <p className="text-xs text-gray-500 dark:text-gray-400">
              PDF (MAX. 4MB)
            </p>
          </div>
          <input
            id="dropzone-file"
            type="file"
            className="hidden"
            value={""}
            onChange={onFileDrop}
            accept={"application/pdf"}
            disabled={fileList.length == 1}
          />
          {fileList.length > 0 && (
            <div className="space-y-2">
              {fileList.map((item, index) => (
                <div
                  key={index}
                  className="flex items-center justify-between bg-white p-4 rounded-lg shadow">
                  <div className="flex-grow">
                    <p className="text-gray-700 p-2">{item.name}</p>
                    <span />
                  </div>
                  <span />
                  <button
                    onClick={() => fileRemove(item)}
                    className="bg-red-400 hover:bg-red-400 rounded-full text-white space-y-2 w-5 h-5 flex items-center justify-center transition duration-300 focus:outline-none"
                    disabled={load}>
                    <span className="text-lg font-semibold">x</span>
                  </button>
                </div>
              ))}
            </div>
          )}
        </label>
      </div>
      <Button
        className="text-large font-normal text-white  bg-violet-400 w-auto enabled:bg-violet-600 disabled:bg-violet-300"
        disabled={!enabledButton}
        onClick={handleSaveToDatabase}>
        Do the magic
      </Button>
      {load && <Spinner />}
    </>
  );
};

export default DropFileInput;
