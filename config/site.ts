export type SiteConfig = typeof siteConfig;

export const siteConfig = {
  name: "Flashlearn",
  description: "Make beautiful websites regardless of your design experience.",
  navItems: [
    {
      label: "Home",
      href: "/",
    },
    {
      label: "About",
      href: "/about",
    },
    {
      label: "Quiz",
      href: "/quiz",
    },
  ],
  navMenuItems: [
    {
      label: "Home",
      href: "/",
    },
    {
      label: "About",
      href: "/about",
    },
    {
      label: "Quiz",
      href: "/quiz",
    },
  ],
  links: {
    upgrade: "#",
  },
  baseUrl: process.env.NEXT_PUBLIC_BACKEND_BASE_API,
};
