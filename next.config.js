/** @type {import('next').NextConfig} */
const nextConfig = {
  env: {
    BASE_URL: process.env.NEXT_PUBLIC_BACKEND_BASE_API,
  },
};

module.exports = nextConfig;
