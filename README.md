# Flaslearn frontend

## AI platform for education.

## Technologies Used

- [Next.js 13](https://nextjs.org/docs/getting-started)
- [NextUI v2](https://nextui.org/)
- [Tailwind CSS](https://tailwindcss.com/)
- [Tailwind Variants](https://tailwind-variants.org)
- [TypeScript](https://www.typescriptlang.org/)
- [Framer Motion](https://www.framer.com/motion/)
- [next-themes](https://github.com/pacocoursey/next-themes)

### Install dependencies

```bash
yarn install
```

### Run the development server

```bash
yarn dev
```

### env.local

```bash
NEXT_PUBLIC_BACKEND_BASE_API=http://localhost:8000
```
