import React from "react";
import { subtitle, title } from "@/components/primitives";

const AboutPage = () => {
  return (
    <section className="w-full flex items-center justify-center py-8 md:py-10">
      <div className="w-full md:w-3/4 lg:w-2/3  text-center">
        <h1 className={title()}>About Flashlearn</h1>
        <p className={subtitle({ class: "" })}>
          Welcome to Flashlearn, your go-to platform for enhancing your studying
          experience through cutting-edge AI technology. We understand the
          challenges of academic life, and our mission is to empower learners
          like you with innovative tools that make studying more effective and
          enjoyable.
        </p>

        <h1 className={title({ color: "violet" })}>Our Vision</h1>
        <p className={subtitle({ class: "" })}>
          At Flashlearn, we envision a world where studying is not just a task
          but a rewarding journey. We believe in the transformative power of
          education and aim to facilitate your learning process by leveraging
          the latest advancements in artificial intelligence.
        </p>

        <h1 className={title()}>What Sets Us Apart</h1>
        <p className={subtitle({ class: "" })}>
          Free-to-Use Software: Flashlearn is committed to providing accessible
          education tools. Our platform is entirely free to use, ensuring that
          quality studying resources are available to everyone.
          <br />
          <br />
          AI-Powered Quizzes and Tests: Harness the power of AI to elevate your
          study sessions. Simply upload any PDF file from your school,
          university, or educational institute, and Flashlearn will generate
          multiple format quizzes and tests tailored to your materials.
          <br />
          <br />
          Personalized Learning: We recognize that each learner is unique.
          Flashlearn adapts to your needs, offering personalized assessments to
          help you focus on areas that require improvement.
        </p>

        <h1 className={title({ color: "violet" })}>How It Works</h1>
        <p className={subtitle({ class: "" })}>
          Upload Your PDFs: Easily upload PDF files of your study materials to
          our platform.
          <br />
          <br />
          AI Magic: Our advanced AI technology analyzes your content and creates
          quizzes and tests designed to reinforce your understanding.
          <br />
          <br />
          Study and Excel: Use the generated quizzes to test your knowledge,
          identify weak points, and excel in your exams with confidence.
        </p>

        <h1 className={title()}>Our Commitment</h1>
        <p className={subtitle({ class: "" })}>
          Flashlearn is more than just a study tool; it&apos;s a commitment to
          your academic success. We strive to provide a user-friendly, dynamic
          platform that evolves with your learning needs. Your journey toward
          excellence starts here.
        </p>

        <h1 className={title({ color: "violet" })}>Join Flashlearn Today</h1>
        <p className={subtitle({ class: "" })}>
          Embark on a transformative learning experience with Flashlearn. Sign
          up for free, upload your study materials, and let our AI technology
          guide you to academic success. Your success is our priority!
        </p>
      </div>
    </section>
  );
};

export default AboutPage;
