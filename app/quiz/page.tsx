"use client";
import { useSearchParams, useRouter } from "next/navigation";
import { FlashcardArray } from "react-quizlet-flashcard";
import { useEffect, useState } from "react";
import { Spinner } from "@nextui-org/spinner";
import axios from "axios";
import { siteConfig } from "@/config/site";
import { subtitle } from "@/components/primitives";
import SelectComponent from "@/app/quiz/components/Select";
import Match from "@/app/quiz/components/Match";

interface QuizItem {
  question: string;
  answer: string;
}

const QuizPage = () => {
  const searchParams = useSearchParams();
  const router = useRouter();
  const [load, setLoad] = useState(true);
  const [questions, setQuestions] = useState<QuizItem[]>([]);
  const [error, setError] = useState("");
  const [matchIsActive, setMatchIsActive] = useState(false);
  const [selected, setSelected] = useState("FlashCards");
  const uid = searchParams.get("uid");
  const items = [
    { label: "FlashCards", value: "FlashCards", onSelect: () => {} },
    {
      label: "MatchGame",
      value: "MatchGame",
      onSelect: () => setMatchIsActive(true),
    },
  ];
  console.log({ questions });
  useEffect(() => {
    setLoad(true);
    const maxRetries = 20;
    let retryCount = 0;
    if (questions.length == 0) {
      const string_questions = localStorage.getItem("quiz");
      if (string_questions !== null) {
        const localQuestions = JSON.parse(string_questions);
        setQuestions(localQuestions);
        setLoad(false);
        return;
      } else if (uid == null) {
        setLoad(false);
      }
    } else {
      setLoad(false);
    }

    const fetchData = async () => {
      setLoad(true);
      try {
        while (retryCount < maxRetries) {
          const response = await axios.get(
            `${siteConfig.baseUrl}/api/v1/analyzer/quizes_view/${uid}`
          );
          const responseData = response.data;
          console.log({ ["analyzer/quizes_view"]: responseData });
          if (
            responseData.open_questions &&
            responseData.open_questions.questions !== null
          ) {
            const parsed_questions = JSON.parse(
              responseData.open_questions.questions
            );
            setQuestions(parsed_questions);
            localStorage.setItem("quiz", JSON.stringify(parsed_questions));
            setLoad(false);
            return;
          }

          retryCount++;
          await new Promise((resolve) =>
            setTimeout(resolve, 4000 + 2000 * retryCount)
          );
        }

        setError("No questions available after multiple retries.");
        setLoad(false);
      } catch (error) {
        setError("Something went wrong");
        setLoad(false);
      }
    };
    if (uid && questions.length === 0) {
      fetchData();
    }
  }, [uid, questions.length]);

  const getCardsData = () => {
    const shuffle = (array: any) => {
      for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
      }
      return array;
    };
    const copiedQuiz = JSON.parse(JSON.stringify(questions));
    const shuffledQuiz = shuffle(copiedQuiz).slice(0, 6);
    // @ts-ignore
    const cardQuestions = shuffledQuiz.map((quiz, index) => ({
      definition: quiz.question,
      term: "",
      matchId: index,
    }));
    // @ts-ignore
    const cardAnswers = shuffledQuiz.map((quiz, index) => ({
      definition: quiz.answer,
      term: "",
      matchId: index,
    }));
    const cardData = cardQuestions.concat(cardAnswers);
    const copiedCardData = JSON.parse(JSON.stringify(cardData));
    shuffle(copiedCardData);
    return copiedCardData;
  };

  const onSelectHandler = (value: string) => {
    setSelected(value);
    if (value === "FlashCards") {
      setMatchIsActive(false);
    } else if (value === "MatchGame") {
      setMatchIsActive(true);
    }
  };

  const onMatchCloseHandler = () => {
    setSelected("FlashCards");
    setMatchIsActive(false);
  };

  return (
    <>
      {load && <Spinner />}
      {error && (
        <div
          className="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
          role="alert">
          <h2 className="font-semibold text-lg">Something went wrong!</h2>
          <p className="text-sm">
            We encountered an error while processing your request.
          </p>
          <button
            className="mt-2 px-4 py-2 bg-red-500 hover:bg-red-600 text-white rounded-full font-semibold transition duration-300 ease-in-out focus:outline-none focus:ring focus:ring-red-400"
            onClick={() => {
              router.push("/");
            }}>
            Try again
          </button>
        </div>
      )}
      {questions.length === 0 && !load && !error && (
        <div className="text-center mt-8">
          <p className="text-xl font-semibold mb-4">
            In order to start learning, please upload your preparation
            materials.
          </p>
          <button
            className="px-4 py-2 bg-blue-500 hover:bg-blue-600 text-white rounded-full font-semibold transition duration-300 ease-in-out focus:outline-none focus:ring focus:ring-blue-400"
            onClick={() => {
              router.push("/");
            }}>
            Upload File
          </button>
        </div>
      )}
      {questions.length !== 0 && (
        <>
          <Match
            isOpen={matchIsActive}
            onClose={onMatchCloseHandler}
            cardsData={getCardsData()}
          />
          <SelectComponent
            items={items}
            selected={selected}
            onSelect={onSelectHandler}
          />
          <FlashcardArray
            onCardFlip={() => {}}
            cards={questions.map((question, ind) => ({
              id: ind,
              frontHTML: (
                <div className="inline-block max-w-lg text-center justify-center dark:text-black">
                  <h1
                    className={`${subtitle({
                      class:
                        "mt-4 dark:text-default-200 light:text-default-600",
                    })}`}>
                    {question.question}&nbsp;
                  </h1>
                </div>
              ),
              backHTML: (
                <div className="inline-block max-w-lg text-center justify-center">
                  <h1
                    className={subtitle({
                      class:
                        "mt-4 dark:text-default-200 light:text-default-600",
                    })}>
                    {question.answer}&nbsp;
                  </h1>
                </div>
              ),
            }))}
          />
        </>
      )}
    </>
  );
};

export default QuizPage;
