import React, { useState } from "react";
import confetti from "canvas-confetti";

interface ModalProps {
  isOpen: boolean;
  onClose: () => void;
  children: React.ReactNode;
}

interface CardProps {
  term: string;
  definition: string;
  matchId: number;
  onCardClick: (matchId: number) => void;
  isSelected: boolean;
  isMatched: boolean;
  isMiss: boolean;
}

interface MatchProps {
  isOpen: boolean;
  onClose: () => void;
  cardsData: CardData[];
}

const Modal: React.FC<ModalProps> = ({ isOpen, onClose, children }) => {
  if (!isOpen) return null;

  return (
    <div className="fixed inset-0 z-50 flex justify-center items-center bg-black bg-opacity-40 p-2">
      <div className="relative bg-white w-11/12 h-5/6 my-4 mx-auto rounded-lg shadow-lg overflow-y-auto">
        {/* Styled close button */}
        <button
          onClick={onClose}
          className="absolute top-0 right-0 mt-4 mr-4 text-black text-2xl leading-none z-50"
          aria-label="Close"
        >
          <span className="bg-transparent text-4xl leading-none">&times;</span>
        </button>
        <div className="p-4">{children}</div>
      </div>
    </div>
  );
};

const Card: React.FC<CardProps> = ({
  term,
  definition,
  matchId,
  onCardClick,
  isSelected,
  isMatched,
  isMiss,
}) => {
  const cardTitleStyle = "text-xl font-semibold mt-4";
  const cardBaseStyle =
    "inline-block max-w-xs text-center p-4 m-2 rounded-lg shadow-lg transform transition duration-300 ease-in-out flex justify-center items-center";
  const selectedStyle = "bg-green-300"; // Style for a selected card
  const matchedStyle = "opacity-0"; // Style for a matched card that will disappear
  const wrongMatchStyle = "bg-red-300"; // Style for a wrong match selection
  const cardStyle = `${cardBaseStyle} ${
    isMatched
      ? matchedStyle
      : isMiss
        ? wrongMatchStyle
        : isSelected
          ? selectedStyle
          : "bg-violet-400 text-white"
  }`;

  return (
    <div className={cardStyle} onClick={() => onCardClick(matchId)}>
      <h3 className={cardTitleStyle}>{term}</h3>
      <p className="mt-2">{definition}</p>
    </div>
  );
};

interface CardData {
  term: string;
  definition: string;
  matchId: number;
}

const Match: React.FC<MatchProps> = ({ isOpen, onClose, cardsData }) => {
  const [firstSelectedCardId, setFirstSelectedCardId] = useState<number | null>(
    null,
  );
  const [secondSelectedCardId, setSecondSelectedCardId] = useState<
    number | null
  >(null);
  const [matchedCardIds, setMatchedCardIds] = useState<number[]>([]);

  const Confetti = () => {
    confetti();
    setTimeout(() => {
      onClose();
      setMatchedCardIds([]);
    }, 1000);
    return <></>;
  };

  const handleCardClick = (index: number) => {
    if (firstSelectedCardId === null) {
      setFirstSelectedCardId(index);
      return;
    }

    setSecondSelectedCardId(index);
    if (cardsData[firstSelectedCardId].matchId === cardsData[index].matchId) {
      setMatchedCardIds((prevState) => {
        return [...prevState, firstSelectedCardId, index];
      });
    }
    setTimeout(() => {
      setFirstSelectedCardId(null);
      setSecondSelectedCardId(null);
    }, 500);
  };

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      {matchedCardIds.length === cardsData.length && <Confetti />}
      {/* Adjusting the grid to fit 16 items */}
      <div className="grid grid-cols-2 sm:grid-cols-3 md:grid-cols-4 lg:grid-cols-5 xl:grid-cols-6 gap-4 p-4 justify-center">
        {cardsData.map((card, index) => {
          const isMatched = matchedCardIds.includes(index);
          const isSelected =
            firstSelectedCardId === index || secondSelectedCardId === index;
          const onCardClick =
            isMatched || isSelected ? () => {} : () => handleCardClick(index);

          return (
            <Card
              key={index}
              term={card.term}
              definition={card.definition}
              matchId={card.matchId}
              onCardClick={onCardClick}
              isSelected={isSelected}
              isMatched={isMatched}
              isMiss={isSelected && secondSelectedCardId !== null}
            />
          );
        })}
      </div>
    </Modal>
  );
};

export default Match;
