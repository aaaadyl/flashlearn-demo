import { Select, SelectItem } from "@nextui-org/select";
import { ChangeEvent } from "react";

interface SelectItemProps {
  label: string;
  value: string;
  onSelect: () => void;
}
interface SelectComponentProps {
  items: SelectItemProps[];
  selected: string;
  onSelect: (value: string) => void;
}
const SelectComponent: React.FC<SelectComponentProps> = ({
  items,
  selected,
  onSelect,
}) => {
  const handleSelectionChange = (e: ChangeEvent<HTMLSelectElement>) => {
    if (e.target.value !== "") {
      onSelect(e.target.value);
    }
  };
  return (
    <div className="flex w-full flex-wrap items-end md:flex-nowrap mb-6 md:mb-0 gap-4">
      <Select
        className="max-w-xs"
        defaultSelectedKeys={[items[0].value]}
        aria-label={"Select quiz type"}
        selectedKeys={[selected]}
        onChange={handleSelectionChange}
      >
        {items.map((item) => (
          <SelectItem key={item.value} value={item.value}>
            {item.label}
          </SelectItem>
        ))}
      </Select>
    </div>
  );
};
export default SelectComponent;
