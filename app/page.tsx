import { title, subtitle } from "@/components/primitives";
import DropFileInput from "@/components/dropzone";

export default function Home() {
  return (
    <section className="flex flex-col items-center justify-center gap-4 py-8 md:py-10">
      <div className="inline-block max-w-lg text-center justify-center">
        <h1 className={title()}>Upload materials and learn with &nbsp;</h1>
        <h1 className={title({ color: "violet" })}>AI quiz!&nbsp;</h1>
        <br />
        <h2 className={subtitle({ class: "mt-4" })}>
          Simplify your preparation with AI driven quizzes that are made only
          using the presentations or documents you upload.
        </h2>
      </div>
      <DropFileInput />
    </section>
  );
}
